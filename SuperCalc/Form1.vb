﻿Public Class Form1

    Dim LeftKeyMargin = 10
    Dim ButtonSide = 70
    Dim InterButtonPad = 5
    Dim TopButtonMargin = 100
    Dim ButtonsPerRow = 3

    Dim register = 0

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblDisplay.Text = ""

        ' Create our numeric buttons
        For index As Integer = 1 To 9
            Dim row As Integer = ((index - 1) \ ButtonsPerRow)
            Dim column As Integer = index - (row * ButtonsPerRow) - 1
            ' System.Diagnostics.Debug.WriteLine("[" & index & "][" & row & "][" & column & "]")
            Dim key = CreateButton(row, column, index, AddressOf DigitClick)
            Me.Controls.Add(key)
        Next

        ' Add zero button
        Dim zeroKey = CreateButton(3, 0, "0", AddressOf DigitClick)
        Me.Controls.Add(zeroKey)

        ' Add Plus Button
        Dim plusKey = CreateButton(3, 1, "+", AddressOf PlusClick)
        Me.Controls.Add(plusKey)

        ' Add Equals Button
        Dim equalsKey = CreateButton(3, 2, "=", AddressOf EqualsClick)
        Me.Controls.Add(equalsKey)

    End Sub

    Private Sub EqualsClick(sender As Object, e As EventArgs)
        Dim total = register + Convert.ToInt32(lblDisplay.Text)
        lblDisplay.Text = total
        register = 0
    End Sub

    Private Sub PlusClick(sender As Object, e As EventArgs)
        register = lblDisplay.Text
        lblDisplay.Text = ""
    End Sub

    Private Sub DigitClick(sender As Object, e As EventArgs)
        lblDisplay.Text = lblDisplay.Text + DirectCast(sender, Button).Text
    End Sub

    Private Function CreateButton(row As Integer, column As Integer, text As String, ByRef handler As EventHandler) As Button
        Dim key = New Button
        key.Size = New Size(ButtonSide, ButtonSide)
        key.Location = New Point(LeftKeyMargin + ((ButtonSide + InterButtonPad) * column), TopButtonMargin + (ButtonSide + InterButtonPad) * row)
        key.Text = text
        AddHandler key.Click, handler
        Return key
    End Function

End Class
